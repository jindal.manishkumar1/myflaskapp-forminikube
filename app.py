from flask import Flask, render_template
import random
import subprocess

app = Flask(__name__)

# list of cat images
images = [
    "https://gitlab.com/jindal.manishkumar1/myflaskapp-forminikube/raw/e115c9d69420f055c86eb10f00b4caba9cb96ea0/images/1.jpg",
    "https://gitlab.com/jindal.manishkumar1/myflaskapp-forminikube/raw/e115c9d69420f055c86eb10f00b4caba9cb96ea0/images/2.jpg",
    "https://gitlab.com/jindal.manishkumar1/myflaskapp-forminikube/raw/e115c9d69420f055c86eb10f00b4caba9cb96ea0/images/3.jpg",
    "https://gitlab.com/jindal.manishkumar1/myflaskapp-forminikube/raw/e115c9d69420f055c86eb10f00b4caba9cb96ea0/images/4.jpg",
    "https://gitlab.com/jindal.manishkumar1/myflaskapp-forminikube/raw/e115c9d69420f055c86eb10f00b4caba9cb96ea0/images/5.jpg"
]

@app.route('/')
def index():
    url = random.choice(images)
    sample_string = "Hello World";
    return render_template('index.html', url=url , str=sample_string)

if __name__ == "__main__":
    app.run(host="0.0.0.0")